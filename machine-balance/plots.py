# Copyright (c) 2014-2020, University of Tennessee. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause
# This program is free software: you can redistribute it and/or modify it under
# the terms of the BSD 3-Clause license.
#
# Plots machine balance and performance data.
# Usage:
#     > ipython
#     In [1]: run plots
#     In [2]: plot_balance ( regexp, save=True )
#

from __future__ import print_function

import numpy
import re
import matplotlib.pyplot as pp
from math import *

from data import names, tbl, j_date, j_name, j_core, j_flop, j_bw, j_bal

pp.rcParams['font.family'] = 'Verdana'

#-------------------------------------------------------------------------------
# Settings

xlim = [ min( 1975, min( tbl[:,j_date] ) ), max( tbl[:,j_date] ) + 5 ]
xticks = range( int(xlim[0]), int(xlim[1]) + 1, 5 )

pp.rcParams['axes.axisbelow'] = True
pp.rcParams['grid.color'] = '#dddddd'

#pp.rcParams['text.usetex'] = 'true'
#pp.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
#pp.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

txt_bbox = dict( facecolor='white', edgecolor='#999999', pad=1 )

#-------------------------------------------------------------------------------
default_regexp = '|'.join((
    r'RaspberryPi$',
    #r'HP Moonshot',
    #Intel Edison
    r'Intel (Core i7|8088|486|Pentium-60|PII\b|PIII$)',  # P4
    r'Core2Duo E6750',
    r'Origin2000-300',
    #r'SGI UV 3000',
    r'CM-5',
    r'VAX-11',
    r'NVIDIA (C1060|M2050|K10|K40|V100|A100$|A100 matrix|H100)',  # P100
    r'AMD (MI|EPYC)',
    r'Y-MP',
    r'C90',
    r'T3E-1200',
    r'Cray X1',
    r'NEC SX-[457]',
    r'Xeon Phi',
    r'K Computer',
    r'Fugaku',
))

CL = ( 0.2, 1.0, 'center', 'left' )
CR = (-0.2, 1.0, 'center', 'right')
TL = ( 0.2, 1.0, 'top',    'left' )
TR = (-0.2, 1.0, 'top',    'right')
BL = ( 0.2, 1.0, 'bottom', 'left' )
BR = (-0.2, 1.0, 'bottom', 'right')
txt_adjust = {
    'Y-MP':          TR,
    'V100':          BL,
    'P100':          TL,
    'A100':          BL,
    'A100 matrix':   CL,
    'KNC':           TL,
    'KNL':           BR,
    'RaspberryPi':   BL,
    'K Computer':    TR,
    'Fugaku':        TL,
    'MI100':         CL,
    'V100':          TR,
    'MI50':          TR,
    'MI250X':        BL,
    'MI250X matrix': TL,
    'Core2Duo':      CR,
    'EPYC 7453':     TL,
    'C90':           BR,
    'H100':          CL,
    'MI100':         CR,
}

#-------------------------------------------------------------------------------
def find_highlights( highlight_regexp ):
    '''
    Returns pair of lists, (highlight, regular), with indices of rows matching
    the highlight_regexp to highlight, and indices of other rows.
    Also prints out summary of highlighted rows.
    '''
    highlight = []
    regular   = []
    for i in range( len(names) ):
        if (re.search( highlight_regexp, names[i] )):
            highlight.append( i )
            print( '%4.0f  %-32s  %10.1f MF/s  %10.1f MW/s  %5.1f balance' %
                    (tbl[i, j_date], names[i], tbl[i, j_flop], tbl[i, j_bw], tbl[i, j_bal]) )
        else:
            regular.append( i )
    return (highlight, regular)
# end

#-------------------------------------------------------------------------------
def shortname( s ):
    '''
    Shorten names for highlight labels.
    '''
    s = re.sub( 'Intel ',  '',    s )
    s = re.sub( 'DEC ',    '',    s )
    s = re.sub( 'T3E-\d+', 'T3E', s )
    s = re.sub( 'i7-\d+',  'i7',  s )
    s = re.sub( 'SGI ',    '',    s )
    s = re.sub( 'Pentium-\d+', 'Pentium', s )
    s = re.sub( 'PII-\d+', 'PII', s )
    s = re.sub( 'Origin2000-\d+', 'Origin2000', s )
    s = re.sub( 'SX-5-16A', 'SX-5', s )
    s = re.sub( '(VAX-\d+)/\d+', r'\1', s )
    s = re.sub( 'Generic ', '', s )
    s = re.sub( ' E\d+', '', s )
    s = re.sub( 'TMC CM-5E', 'CM-5E', s )
    s = re.sub( 'Cray (T3E|T3D|C90|Y-MP)', r'\1', s )
    s = re.sub( 'Xeon Phi \S+ \(Knights Corner\)', 'KNC', s )
    s = re.sub( 'Xeon Phi \S+ \(Knights Landing\)', 'KNL', s )
    s = re.sub( 'NVIDIA ', '', s )
    s = re.sub( 'AMD ', '', s )
    return s
# end

#-------------------------------------------------------------------------------
def plot_per_core( highlight_regexp=default_regexp, save=False ):
    '''
    Plots performance per core (flop/s and bandwidth).
    '''
    pp.figure( 4, [7.5, 5], tight_layout={'pad': 1} )
    pp.clf()

    # Separate highlighted and regular data points.
    (hi, reg) = find_highlights( highlight_regexp )
    for i in hi:
        name = shortname( names[i] )
        if (name in txt_adjust):
            (dx, dy, valign, halign) = txt_adjust[ name ]
        else:
            (dx, dy, valign, halign) = TR
        pp.text( tbl[i, j_date], tbl[i, j_flop] / tbl[i, j_core], name,
                 fontsize=9, ha=halign,  va=valign,
                 bbox=txt_bbox, zorder=tbl[i, j_date] )
    # end

    pp.semilogy( tbl[reg, j_date], tbl[reg, j_bw  ] / tbl[reg, j_core], 'bs', ms=2, mew=0, zorder=1 )
    pp.semilogy( tbl[reg, j_date], tbl[reg, j_flop] / tbl[reg, j_core], 'rd', ms=2, mew=0, zorder=2 )
    pp.semilogy( tbl[hi,  j_date], tbl[hi,  j_flop] / tbl[hi,  j_core], 'rd', ms=5, mew=1, zorder=4, label='Mflop/s' )
    pp.semilogy( tbl[hi,  j_date], tbl[hi,  j_bw  ] / tbl[hi,  j_core], 'bs', ms=5, mew=1, zorder=3, label='MW/s'    )

    pp.title( 'Performance per core' )
    pp.ylim( 5e-2, 2e5 )
    pp.xlim( xlim )
    pp.xticks( xticks )
    pp.ylabel( 'Mflop/s or MW/s' )
    pp.xlabel( 'year' )
    pp.legend( loc='best' )
    pp.grid()

    if (save):
        file = 'per_core.pdf'
        print( 'saving', file )
        pp.savefig( file, transparent=True )
# end

#-------------------------------------------------------------------------------
def plot_flops( highlight_regexp=default_regexp, clear=True, save=False ):
    '''
    Plots flop/s performance.
    '''
    if (clear):
        pp.figure( 2, [7.5, 5], tight_layout={'pad': 1} )
        pp.clf()

    # Separate highlighted and regular data points.
    (hi, reg) = find_highlights( highlight_regexp )
    for i in hi:
        name = shortname( names[i] )
        if (name in txt_adjust):
            (dx, dy, valign, halign) = txt_adjust[ name ]
        else:
            (dx, dy, valign, halign) = TR
        pp.text( tbl[i, j_date], tbl[i, j_flop] * 1e6, name,
                 fontsize=9, ha=halign,  va=valign,
                 bbox=txt_bbox, zorder=tbl[i, j_date], color='r', alpha=1.0 )
    # end

    pp.semilogy( tbl[reg, j_date], tbl[reg, j_flop] * 1e6, 'rd', ms=2, mew=0, zorder=2 )
    pp.semilogy( tbl[hi,  j_date], tbl[hi,  j_flop] * 1e6, 'rd', ms=5, mew=1, zorder=4, label='flop/s' )

    pp.title( 'Floating point performance (double-precision)' )
    pp.ylim( 5e4, 1e15 )
    pp.xlim( xlim )
    pp.xticks( xticks )
    pp.yticks( [1e6, 1e9, 1e12, 1e15], ['$10^{6}$\nmega', '$10^{9}$\ngiga', '$10^{12}$\ntera', '$10^{15}$\npeta'] )
    pp.ylabel( 'flop/s' )
    pp.xlabel( 'year' )
    #pp.legend( loc='best' )
    pp.grid()

    if (save):
        file = 'flops.pdf'
        print( 'saving', file )
        pp.savefig( file, transparent=True )
# end

#-------------------------------------------------------------------------------
def plot_bw( highlight_regexp=default_regexp, clear=True, save=False ):
    '''
    Plots bandwidth performance.
    Scale bw in MW/s by 8e6 to get byte/s.
    '''
    if (clear):
        pp.figure( 3, [7.5, 5], tight_layout={'pad': 1} )
        pp.clf()

    # Separate highlighted and regular data points.
    (hi, reg) = find_highlights( highlight_regexp )
    for i in hi:
        name = shortname( names[i] )
        if (name in txt_adjust):
            (dx, dy, valign, halign) = txt_adjust[ name ]
        else:
            (dx, dy, valign, halign) = TR
        pp.text( tbl[i, j_date], tbl[i, j_bw] * 8e6, name,
                 fontsize=9, ha=halign,  va=valign,
                 bbox=txt_bbox, zorder=tbl[i, j_date], color='b', alpha=1.0 )
    # end

    pp.semilogy( tbl[reg, j_date], tbl[reg, j_bw  ] * 8e6, 'bs', ms=2, mew=0, zorder=1 )
    pp.semilogy( tbl[hi,  j_date], tbl[hi,  j_bw  ] * 8e6, 'bs', ms=5, mew=1, zorder=3, label='byte/s' )

    pp.title( 'Memory bandwidth' )
    pp.ylim( 5e4, 1e15 )
    pp.xlim( xlim )
    pp.xticks( xticks )
    pp.yticks( [1e6, 1e9, 1e12, 1e15], ['$10^{6}$\nmega', '$10^{9}$\ngiga', '$10^{12}$\ntera', '$10^{15}$\npeta'] )
    pp.ylabel( 'byte/s' )
    pp.xlabel( 'year' )
    #pp.legend( loc='best' )
    pp.grid()

    if (save):
        file = 'bw.pdf'
        print( 'saving', file )
        pp.savefig( file, transparent=True )
# end

#-------------------------------------------------------------------------------
def plot_performance( highlight_regexp=default_regexp, save=False ):
    '''
    Plots total node performance (flop/s and bandwidth).
    '''
    pp.figure( 2, [7.5, 5], tight_layout={'pad': 1} )
    pp.clf()

    plot_bw(    highlight_regexp, clear=False )
    plot_flops( highlight_regexp, clear=False )

    pp.title( 'Total performance' )
    pp.ylabel( 'DP flop/s or byte/s' )
    pp.legend( loc='best' )

    if (save):
        file = 'performance.pdf'
        print( 'saving', file )
        pp.savefig( file, transparent=True )
# end

#-------------------------------------------------------------------------------
def plot_rate( rate, years, ymin, label_year, label_adjust, label_rotation ):
    '''
    Used in plot_balance() to plot growth lines.
    Plots line increasing at rate (> 1 is increase) over given years,
    starting from ymin.
    Adds label at label_year, with adjustment y *= label_adjust,
    and given label_rotation.
    '''
    ymax = ymin * rate**(years[1] - years[0])
    pp.semilogy( years, [ymin, ymax], '-', color='#cccccc', lw=3, zorder=-2 )
    label_y = ymin * rate**(label_year - years[0]) * label_adjust
    percent = 100 * (rate - 1)
    pp.text( label_year, label_y, '%.0f%% per year' % (percent),
             ha='left', rotation=label_rotation )
# end

#-------------------------------------------------------------------------------
def plot_balance( highlight_regexp=default_regexp, save=False ):
    '''
    Plots machine balance (flop per read).
    '''
    pp.figure( 1, [7.5, 5], tight_layout={'pad': 1} )
    pp.clf()

    # Separate highlighted and regular data points.
    (hi, reg) = find_highlights( highlight_regexp )
    for i in hi:
        name = shortname( names[i] )
        if (name in txt_adjust):
            (dx, dy, valign, halign) = txt_adjust[ name ]
        else:
            (dx, dy, valign, halign) = TR
        pp.text( tbl[i, j_date] + dx, tbl[i, j_bal]*dy, name,
                 fontsize=9, ha=halign,  va=valign,
                 bbox=txt_bbox, zorder=tbl[i, j_date], alpha=1.0 )
    # end

    pp.semilogy( tbl[reg, j_date], tbl[reg, j_bal], 'o', ms=2, mew=0, zorder=1, color='#cc0000' )
    pp.semilogy( tbl[hi,  j_date], tbl[hi,  j_bal], 'o', ms=5, mew=1, zorder=2, color='#cc0000', mec='k' )

    # 30% and 15% increases bound data, with few outliers.
    plot_rate( 1.30, [1975, xlim[1]], 2e-1, 1983, 1.5,  52 )
    plot_rate( 1.20, [1979, xlim[1]], 2e-1, 1989, 0.75, 35 )
    plot_rate( 1.15, [1985, xlim[1]], 2e-1, 1989, 0.75, 35 )

    # 18% increase fits Y-MP and C90 to V100 nicely.
    #plot_rate( 1.18, [1982.5, 2020], 2e-1, 1984, 0.8,  35 )

    pp.title( 'Machine balance\n(double-precision floating point operations per read)' )
    pp.ylim( 2e-1, 3e2 )
    pp.xlim( xlim )
    pp.xticks( xticks )
    # todo: matplotlib doesn't support \text
    pp.ylabel( r'$\dfrac{{flop/s}}{{word/s}}$' )
    pp.xlabel( 'year' )
    pp.yticks( (1, 10, 100), ('1', '10', '100') )
    pp.grid()

    if (save):
        file = 'machine-balance.pdf'
        print( 'saving', file )
        pp.savefig( file, transparent=True )
# end

#-------------------------------------------------------------------------------
def plot_all( **kwargs ):
    '''
    Passes arguments on to all the plot_* routines, e.g.,
        plot_all( save=True )
    '''
    #plot_per_core( **kwargs )
    plot_flops   ( **kwargs )
    plot_bw      ( **kwargs )
    plot_balance ( **kwargs )
# end

usage = '''
--------------------------------------------------------------------------------
Usage:
    plot_all( regexp=default_regexp, save=True )
    or
    plot_per_core( regexp=default_regexp, save=True )
    plot_flops   ( regexp=default_regexp, save=True )
    plot_bw      ( regexp=default_regexp, save=True )
    plot_balance ( regexp=default_regexp, save=True )

Variable `regexp` is preset to highlight a variety of interesting architectures.
Runs `plot_balance( regexp, save=False )` by default.

Plot script by Mark Gates, Innovative Computing Laboratory, University of Tennessee
Copyright 2014-2022. SPDX-License-Identifier: BSD-3-Clause.
https://bitbucket.org/mgates3/plots/src/master/machine-balance/

Data from:
* Vendor specs

* STREAM benchmark, John D. McCalpin
  https://www.cs.virginia.edu/stream/standard/Balance.html

* Wikipedia
  https://en.wikipedia.org/wiki/Nvidia_Tesla
  https://en.wikipedia.org/wiki/Xeon_Phi
'''

#-------------------------------------------------------------------------------
# main
print( usage )
plot_all()
