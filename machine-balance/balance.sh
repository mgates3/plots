#!/bin/sh
#
# Given updated data in balance-raw.txt from
# https://www.cs.virginia.edu/stream/standard/Balance.html
# this does some post-processing, then compares it with balance.csv
# for easy updating.

set -v

perl -pe 's/^([\d\.]+)( +\S+)( +\d+)( +\d+\.\d+)( +\d+\.\d+)( +\d+\.\d+) +data/$1,$2,$3,$4,$5,$6/; s/_/ /g; s/^([^\d])/# $1/; s/T3D-/T3D/; s/RS-6000/RS6000/;' \
    balance-raw.txt | sort > balance-post.txt

opendiff balance-post.txt balance.csv
