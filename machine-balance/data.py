# Copyright (c) 2014-2020, University of Tennessee. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause
# This program is free software: you can redistribute it and/or modify it under
# the terms of the BSD 3-Clause license.
#
# Reads balance.csv and parses into numpy tables for plots.py to use.

from __future__ import print_function

import numpy
import re

#-------------------------------------------------------------------------------
# Settings

# Indices in csv file and tbl array.
j_date = 0
j_name = 1  # 0 in tbl array.
j_core = 2
j_flop = 3
j_bw   = 4
j_bal  = 5

#-------------------------------------------------------------------------------
def date_str2year( date ):
    '''
    Converts "yyyy.mm.dd" or "yyyy" to floating point year + month/12.
    '''
    m = re.match( '^(\d\d\d\d)\.(\d\d)\.(\d\d)$', date )
    if (m):
        date = int( m.group(1) ) + int( m.group(2) )/12.
    else:
        m = re.match( '^(\d\d\d\d)$', date )
        date = int( m.group(1) )
    return date
# end

#-------------------------------------------------------------------------------
# Load data from balance.csv file.
raw = numpy.loadtxt( 'balance.csv', dtype=str, skiprows=5, comments='#', delimiter=', ' )

# For each name, find row with highest flop/s performance.
# uniq is map from name => (flop/s performance, index i) pair.
uniq = {}
for i in range( raw.shape[0] ):
    perf = float( raw[i, j_flop] )
    name = raw[i, j_name].strip()
    raw[i, j_name] = name
    if (name not in uniq or uniq[name][0] < perf):
        uniq[name] = ( perf, i )
# end
index = list( map( lambda x: x[1], uniq.values() ) )
raw_best = raw[index, :]

# Store names in array of strings.
names = raw_best[:, j_name]

# Store other columns in array of floats. j_name column is left 0.
tbl = numpy.zeros( raw_best.shape )
tbl[:, j_date] = list( map( date_str2year, raw_best[:, j_date] ) )
for j in (j_flop, j_bw):
    tbl[:, j] = raw_best[:, j].astype(float)
tbl[:, j_bal] = tbl[:, j_flop] / tbl[:, j_bw]  # machine balance
